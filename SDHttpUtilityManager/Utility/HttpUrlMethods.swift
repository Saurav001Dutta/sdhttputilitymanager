//
//  HttpUrlMethods.swift
//  Truckin Digital Manager
//
//  Created by Saurav Dutta on 09/06/21.
//  Copyright © 2021 Truckin Systems LLC. All rights reserved.
//

import Foundation

public enum HttpUrlMethods : String
{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
