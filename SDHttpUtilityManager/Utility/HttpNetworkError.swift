//
//  HttpNetworkError.swift
//  Truckin Digital Manager
//
//  Created by Saurav Dutta on 09/06/21.
//  Copyright © 2021 Truckin Systems LLC. All rights reserved.
//

import Foundation

public struct HttpNetworkError : Error
{
    let reason: String?
    let httpStatusCode: Int?
    let requestUrl: URL?
    let requestBody: String?
    let serverResponse: String?

    init(withServerResponse response: Data? = nil, forRequestUrl url: URL, withHttpBody body: Data? = nil, errorMessage message: String, forStatusCode statusCode: Int?)
    {
        self.serverResponse = response != nil ? String(data: response!, encoding: .utf8) : nil
        self.requestUrl = url
        self.requestBody = body != nil ? String(data: body!, encoding: .utf8) : nil
        self.httpStatusCode = statusCode
        self.reason = message
    }
}

enum HttpStatusCode : Int
{
    case STATUS_CODE_REQUEST_TIMEOUT         = 1001
    case STATUS_CODE_SUCCESS                 = 200
    case STATUS_CODE_SUCCESS_CREATED         = 201
    case STATUS_CODE_UNAUTHORIZED_ACCESS     = 400
    case STATUS_CODE_UNABLE_TO_CONNECT       = 404
    case STATUS_CODE_INTERNAL_SERVER_ERROR   = 500
    case STATUS_CODE_NIL_DATA                = 1501
    case STATUS_CODE_UNKNOWN_ERROR           = 1502
}
