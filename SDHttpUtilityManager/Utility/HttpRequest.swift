//
//  HttpRequest.swift
//  Truckin Digital Manager
//
//  Created by Saurav Dutta on 09/06/21.
//  Copyright © 2021 Truckin Systems LLC. All rights reserved.
//

import Foundation

protocol Request
{
    var url: URL { get set }
    var method: HttpUrlMethods {get set}
}

public struct HttpRequest : Request
{
    var url: URL
    var method: HttpUrlMethods
    var requestBody: Data? = nil

    init(withUrl url: URL, forHttpMethod method: HttpUrlMethods, requestBody: Data? = nil) {
        self.url = url
        self.method = method
        self.requestBody = requestBody != nil ? requestBody : nil
    }
    
}
